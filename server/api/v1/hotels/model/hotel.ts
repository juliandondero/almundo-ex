"use strict";

class Hotel {
	name: string;
	stars: number;
	price: number;
  url_images:Array<string>;
  recommended:boolean;
  discount:number;

 	constructor(name: string,stars:number,price:number,url_images:Array<string>,recommended:boolean,disccount:number) {
        this.name = name;
        this.stars = stars;
        this.price = price;
        this.url_images=url_images;
        this.recommended= recommended;
        this.discount= disccount;
    }

}

export default Hotel;
