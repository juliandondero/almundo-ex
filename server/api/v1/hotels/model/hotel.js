"use strict";
var Hotel = (function () {
    function Hotel(name, stars, price, url_images, recommended, disccount) {
        this.name = name;
        this.stars = stars;
        this.price = price;
        this.url_images = url_images;
        this.recommended = recommended;
        this.discount = disccount;
    }
    return Hotel;
}());
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = Hotel;
//# sourceMappingURL=hotel.js.map