"use strict";

import * as express from 'express';
import HotelDAO from '../dao/hotel-dao';



class HotelController {

  static getAll(req:express.Request, res:express.Response) {

    return HotelDAO.findAll()
    .then(hotels => res.status(200).json({hotels:hotels}))
      .catch(error => res.status(400).json(error));
  }

}

export default HotelController;
