"use strict";
var hotel_dao_1 = require('../dao/hotel-dao');
var HotelController = (function () {
    function HotelController() {
    }
    HotelController.getAll = function (req, res) {
        return hotel_dao_1.default.findAll()
            .then(function (hotels) { return res.status(200).json({ hotels: hotels }); })
            .catch(function (error) { return res.status(400).json(error); });
    };
    return HotelController;
}());
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = HotelController;
//# sourceMappingURL=hotel-controller.js.map