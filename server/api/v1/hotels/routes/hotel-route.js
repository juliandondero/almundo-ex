"use strict";
var hotel_controller_1 = require('../controller/hotel-controller');
var HotelRoutes = (function () {
    function HotelRoutes() {
    }
    HotelRoutes.init = function (router) {
        router
            .route('/api/v1/hotels')
            .get(hotel_controller_1.default.getAll);
    };
    return HotelRoutes;
}());
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = HotelRoutes;
//# sourceMappingURL=hotel-route.js.map