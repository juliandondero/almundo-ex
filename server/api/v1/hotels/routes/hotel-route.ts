"use strict";

import * as express from 'express';
import HotelController from '../controller/hotel-controller';

export default class HotelRoutes {

	static init(router:express.Router) {
		router
		.route('/api/v1/hotels')
		.get(HotelController.getAll);
	}
}
