"use strict";
var Promise = require('bluebird');
var hotel_1 = require('../model/hotel');
var HotelDAO = (function () {
    function HotelDAO() {
    }
    HotelDAO.findAll = function () {
        return new Promise(function (resolve, reject) {
            // mock obtención datos de base de datos.
            resolve([new hotel_1.default("Hotel Emperador", 3, 1596, ['/common/images/hotel1.1.jpg'], false, 8),
                new hotel_1.default("Petit Palace San Bernardo", 4, 2145, ['/common/images/hotel2.1.jpg'], false, null),
                new hotel_1.default("Hotel Nuevo Boston", 2, 861, ['/common/images/hotel3.1.jpg'], true, 10)]);
        });
    };
    return HotelDAO;
}());
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = HotelDAO;
//# sourceMappingURL=hotel-dao.js.map