"use strict";

import * as Promise from 'bluebird';
import Hotel from '../model/hotel';
import * as _ from 'lodash';

class HotelDAO  {

  static findAll():Promise<any> {
    return new Promise((resolve:Function,reject:Function) => {

      // mock obtención datos de base de datos.
      resolve([new Hotel("Hotel Emperador",3,1596,['/common/images/hotel1.1.jpg'],false,8),
        new Hotel("Petit Palace San Bernardo",4,2145,['/common/images/hotel2.1.jpg'],false,null),
        new Hotel("Hotel Nuevo Boston",2,861,['/common/images/hotel3.1.jpg'],true,10)])
    }
    );
  }

}

export default HotelDAO;
