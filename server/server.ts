/// <reference path="../typings/index.d.ts" />

'use strict';

if (process.env.NODE_ENV === 'production')
    require('newrelic');

var PORT = process.env.PORT || 3333;

import * as express from 'express';
import * as os from 'os';
import * as http from 'http';
import * as fs from 'fs';
import {RoutesConfig} from './config/routes.conf';
import {Routes} from './routes/index';

const app = express();

RoutesConfig.init(app);
Routes.init(app, express.Router());

http.createServer(app)
     .listen(PORT, () => {
     	console.log(`******************************************************************************`);
     	console.log(`******************************************************************************`);
     	console.log(`******                                                                  ******`);
     	console.log(`******          Aplicacion corriendo en: http://localhost:${3000}          ******`);
     	console.log(`******                                                                  ******`);
     	console.log(`******************************************************************************`);
     	console.log(`******************************************************************************`);
       console.log(`enviroment: ${process.env.NODE_ENV}`);
     });
