/// <reference path="../typings/index.d.ts" />
'use strict';
if (process.env.NODE_ENV === 'production')
    require('newrelic');
var PORT = process.env.PORT || 3333;
var express = require('express');
var http = require('http');
var routes_conf_1 = require('./config/routes.conf');
var index_1 = require('./routes/index');
var app = express();
routes_conf_1.RoutesConfig.init(app);
index_1.Routes.init(app, express.Router());
http.createServer(app)
    .listen(PORT, function () {
    console.log("******************************************************************************");
    console.log("******************************************************************************");
    console.log("******                                                                  ******");
    console.log("******          Aplicacion corriendo en: http://localhost:" + 3000 + "          ******");
    console.log("******                                                                  ******");
    console.log("******************************************************************************");
    console.log("******************************************************************************");
    console.log("enviroment: " + process.env.NODE_ENV);
});
//# sourceMappingURL=server.js.map