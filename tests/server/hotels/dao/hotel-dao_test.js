import HotelDAO from '../../../../server/api/v1/hotels/dao/hotel-dao';

var expect = require('chai').expect;

describe('HotelDAO', () => {

  describe('findAll', () => {

    it('should get all hotels', (done) => {
      let _onSuccess = hotels => {
      expect(hotels).to.be.defined;
      expect(hotels[0]).to.have.property('name').and.to.equal('Hotel Emperador');
    done();
  }

    let _onError = (err) => {
      expect(true).to.be.false; // should not come here
    }

  HotelDAO.findAll()
      .then(_onSuccess)
      .catch(_onError);
  })

})


})
