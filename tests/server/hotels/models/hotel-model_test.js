import Hotel from '../../../../server/api/v1/hotels/model/hotel';
var expect = require('chai').expect;

describe('Hotel-Model', () => {

  describe('constructor', () => {

  it('unit test hotel model', (done) => {
    var hotel_name = "Hotel Emperador";

    var hotel_stars = 3;
    var hotel_price = 4;
    var url_images = ['/common/images/hotel1.1.jpg'];
    var recommended = false;
    var discount = 8;


    var instance = new Hotel(hotel_name,hotel_stars,hotel_price,url_images,recommended,discount);

    expect(instance.name).to.be.equal(hotel_name);
    expect(instance.stars).to.be.equal(hotel_stars);
    expect(instance.price).to.be.equal(hotel_price);
    expect(instance.url_images[0]).to.be.equal(url_images[0]);
    expect(instance.recommended).to.be.equal(recommended);
    expect(instance.discount).to.be.equal(discount);

    done();
})

})


})
