describe('component: sortMethodsFilter', function() {
  var service;

  beforeEach(function () {

    module('common-services');

    inject([
      'SortMethods', function (SortMethods) {
        service = SortMethods;
      }
    ]);


    it('Defult sorting must be defined', function () {
      var default_filter = {
        id: 1,
        label: 'precio',
        propertyName: 'price',
        reverse: false
      };

      expect(service.getDefault().id).toEqual(default_filter.id);
      expect(service.getDefault().label).toEqual(default_filter.label);
      expect(service.getDefault().propertyName).toEqual(default_filter.propertyName);
      expect(service.getDefault().reverse).toEqual(default_filter.reverse);
    });

    it('Defult sorting must be defined', function () {

      expect(service.getAll().length).toEqual(2);

    });


  });

});
