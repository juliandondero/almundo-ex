describe('component: hotelsRoute', function() {
  var $componentController, $httpBackend;

  beforeEach(module('hotels-route'));
  beforeEach(inject(function(_$componentController_,_$httpBackend_) {
    $componentController = _$componentController_;
    $httpBackend = _$httpBackend_;
  }));

  it('should expose a default filters', function() {
    // Here we are passing actual bindings to the component
    var bindings = {};
    var ctrl = $componentController('hotelsRoute', null, bindings);

    expect(ctrl.searchFilter).toBeDefined();
    expect(ctrl.sortMethodSelected).toBeDefined();
    expect(ctrl.sortMethodSelected.id).toBe(1);
    expect(ctrl.sortMethodSelected.label).toBe('precio');
    expect(ctrl.sortMethodSelected.propertyName).toBe('price');
    expect(ctrl.sortMethodSelected.reverse).toBe(false);


  });

  it('should expose a hotels', function() {

    var hotels_mock = {hotels:[{id:1,name:'hotel_1'}]};
    $httpBackend.expectGET('/api/v1/hotels').respond(200,hotels_mock );

    // Here we are passing actual bindings to the component
    var bindings = {};
    var ctrl = $componentController('hotelsRoute', null, bindings);

    $httpBackend.flush();

    expect(ctrl.hotels).toEqual(hotels_mock.hotels);


  });


});
