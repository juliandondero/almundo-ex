describe('component: rangeFilterBox', function() {
  var $componentController;

  beforeEach(module('range-filter-box-component'));
  beforeEach(inject(function(_$componentController_) {
    $componentController = _$componentController_;

  }));

  it('should slider defined', function() {
    var applyCallbackSpy= jasmine.createSpy('applyCallback');

    var default_min=10;
    var default_max=90;
    // Here we are passing actual bindings to the component
    var bindings = {
      applyCallback:applyCallbackSpy,
      rangeMin:1,
      rangeMax:100,
      prefix:'$',
      defaultMinValue:default_min,
      defaultMaxValue:default_max
    };

    var ctrl = $componentController('rangeFilterBox', null, bindings);

    expect(ctrl.slider.minValue).toBe(bindings.defaultMinValue);
    expect(ctrl.slider.maxValue).toBe(bindings.defaultMaxValue);
    expect(ctrl.slider.options.floor).toBe(bindings.rangeMin);
    expect(ctrl.slider.options.ceil).toBe(bindings.rangeMax);


  });



  it('should call apply method', function() {
    var applyCallbackSpy= jasmine.createSpy('applyCallback');

    var default_min=10;
    var default_max=90;
    // Here we are passing actual bindings to the component
    var bindings = {
      applyCallback:applyCallbackSpy,
      rangeMin:1,
      rangeMax:100,
      prefix:'$',
      defaultMinValue:default_min,
      defaultMaxValue:default_max
    };

    var ctrl = $componentController('rangeFilterBox', null, bindings);

    ctrl.apply();

    expect(applyCallbackSpy).toHaveBeenCalledWith({
      minValue:default_min,
      maxValue:default_max
    });

  });



});
