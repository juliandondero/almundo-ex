;(function(ng) {
  'use strict';

  AppConfig.$inject=['$locationProvider'];

  function AppConfig($locationProvider){
    $locationProvider.html5Mode(true);
  }

  ng.module('almundo-ex').config(AppConfig);
}(window.angular));
