;(function(ng) {
  'use strict';

  ng.module('almundo-ex', [
    'ngResource',
    'ngRoute',
    'ngMessages',
    'ui.bootstrap',
    'rzModule',

    // componentes propios
    'common-services',
    'header-component',
    'breadcrumbs-component',
    'hotels-route',
    'search-info-component',
    'sorting-bar-component',
    'map-component',
    'hotel-preview-component',
    'common-resources',
    'text-filter-box-component',
    'range-filter-box-component',
    'common-filters',
    'stars-filter-box-component',
    'collapsable-box-component',
    'responsive-collapsable-box-component'

  ]);

}(window.angular));
