;(function(ng) {
  'use strict';

  AppRoutes.$inject=['$routeProvider'];

  function AppRoutes($routeProvider){
    $routeProvider
      .when('/home', {
        template: '<h1>Home</h1>'
      })
      .otherwise({
        redirectTo: '/hotels'
      });
  }

  ng.module('almundo-ex').config(AppRoutes);

}(window.angular));
