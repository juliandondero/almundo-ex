;(function(ng) {
  'use strict';

  HotelPreviewCtrl.$inject=[];

  function HotelPreviewCtrl(){
    var vm=this;

    vm.myInterval = 5000;
    vm.noWrapSlides = false;
    vm.active = true;


  }

  ng.module('hotel-preview-component').component('hotelPreview',{
    controller:HotelPreviewCtrl,
    controllerAs:'vm',
    templateUrl:'almundo-ex/hotel-preview-component/hotel-preview-component.template.html',
    bindings:{
      hotel:'<'
    }
  });

}(window.angular));
