;(function(ng) {
  'use strict';

  HotelFactory.$inject=['$resource'];

  function HotelFactory($resource){

    return $resource('/api/v1/hotels', null,
      {
        'query': { method:'GET' }
      });
  }

  ng.module('common-resources').factory('Hotel',HotelFactory);

}(window.angular));
