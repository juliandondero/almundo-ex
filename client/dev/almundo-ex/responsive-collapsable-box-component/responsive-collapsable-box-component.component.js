;(function(ng) {
  'use strict';

  ResponsiveCollapsableBoxCtrl.$inject=[];

  function ResponsiveCollapsableBoxCtrl(){

    var vm=this;
    vm.visible=null;

    activate();

    function activate(){
      vm.visible = true;
    }

    vm.toggleVisible=function(){
      vm.visible = !vm.visible;
    }


  }

  ng.module('responsive-collapsable-box-component').component('responsiveCollapsableBox',{
    controller:ResponsiveCollapsableBoxCtrl,
    controllerAs:'vm',
    templateUrl:'almundo-ex/responsive-collapsable-box-component/responsive-collapsable-box-component.template.html',
    transclude:true,
    bindings:{
      title:'@'
    }
  });

}(window.angular));
