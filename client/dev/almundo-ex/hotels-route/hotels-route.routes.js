;(function(ng) {
  'use strict';

  HotelsRoute.$inject=['$routeProvider'];

  function HotelsRoute($routeProvider){
    $routeProvider.when('/hotels',{
      template: '<hotels-route></hotels-route>'
    });
  }

  ng.module('hotels-route').config(HotelsRoute);

}(window.angular));
