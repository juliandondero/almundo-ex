;(function(ng) {
  'use strict';

  ng.module('hotels-route', [
    'ngRoute',
    'search-info-component',
    'sorting-bar-component',
    'map-component',
    'common-resources'
  ]);

}(window.angular));
