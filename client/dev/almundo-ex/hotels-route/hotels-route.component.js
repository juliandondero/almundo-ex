;(function(ng) {
  'use strict';

  HotelsRouteCtrl.$inject=['Hotel','$log','SortMethods'];

  function HotelsRouteCtrl(Hotel,$log,SortMethods){
    var vm = this;
    vm.searchFilter = null;
    vm.sortMethodSelected=null;
    vm.hotels=null;

    activate();

    function activate(){

      //mock del filtro de busqueda
      vm.searchFilter= {
        location:"Madrid",
        start_date:"16/11/2016",
        end_date:"26/11/2016",
        passengers:2,
        name:'',
        minRange:300,
        maxRange:3000,
        minPrice:500,
        maxPrice:2800,
        stars:[1,2,3,4,5]
      };

      vm.sortMethodSelected=SortMethods.getDefault();

      Hotel.query({},function(hotels,headers){

        vm.hotels= hotels.hotels;

      },function(errors){
        $log.error(errors);
      });

    }

    vm.filterByName=function(text){
      alert(text); // TODO
    }

    vm.filterByRange=function(minValue,maxValue){
      vm.searchFilter.minPrice=minValue;
      vm.searchFilter.maxPrice=maxValue;
    }

    vm.filterByStars=function(stars){
      vm.searchFilter.stars = stars;
    }

  }

  ng.module('hotels-route').component('hotelsRoute',{
    controller:HotelsRouteCtrl,
    controllerAs:'vm',
    templateUrl:'almundo-ex/hotels-route/hotels-route.template.html',
    bindings:{}
  });

}(window.angular));
