;(function(ng) {
  'use strict';

  SortingBarCtrl.$inject=['SortMethods'];

  function SortingBarCtrl(SortMethods){
    var vm = this;

    vm.sortOptions=SortMethods.getAll();
  }

  ng.module('sorting-bar-component').component('sortingBar',{
    controller:SortingBarCtrl,
    controllerAs:'vm',
    templateUrl:'almundo-ex/sorting-bar-component/sorting-bar-component.template.html',
    bindings:{
      sortMethodSelected:'='
    }
  });

}(window.angular));
