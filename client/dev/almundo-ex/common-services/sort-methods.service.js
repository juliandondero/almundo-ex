;(function(ng) {
  'use strict';

  SortMethods.$inject=[];

  function SortMethods(){

    var _sortMethods = [
      {
        id:1,
        label:'precio',
        propertyName:'price',
        reverse:false
      },
      {
        id:2,
        label:'estrellas',
        propertyName:'stars',
        reverse:true
      }
    ];

    return {
      getAll: function(){
        return _sortMethods;
      },
      getDefault:function(){
        return _sortMethods[0];
      }
    }

  }

  ng.module('common-services').service('SortMethods',SortMethods);

}(window.angular));
