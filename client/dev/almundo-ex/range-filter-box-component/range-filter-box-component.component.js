;(function(ng) {
  'use strict';

  RangeFilterBoxCtrl.$inject=[];

  function RangeFilterBoxCtrl(){

    var vm = this;
    vm.slider=null;

    activate();

    function activate(){
      vm.slider = {
        minValue: parseInt(vm.defaultMinValue),
        maxValue: parseInt(vm.defaultMaxValue),
        options: {
          floor: parseInt(vm.rangeMin),
          ceil: parseInt(vm.rangeMax),
          translate: function(value) {
            return vm.prefix + value;
          },
          step:20,
          onEnd:function(sliderId, modelValue, highValue, pointerType){
            vm.apply();
          }
        }
      };
    }


    vm.apply=function(){

      vm.applyCallback({minValue:vm.slider.minValue,maxValue:vm.slider.maxValue});
      vm.value=null;
    }


  }

  ng.module('range-filter-box-component').component('rangeFilterBox',{
    controller:RangeFilterBoxCtrl,
    controllerAs:'vm',
    templateUrl:'almundo-ex/range-filter-box-component/range-filter-box-component.template.html',
    bindings: {
      applyCallback:'&',
      rangeMin:'@',
      rangeMax:'@',
      prefix:'@',
      defaultMinValue:'@',
      defaultMaxValue:'@'
    }
  });

}(window.angular));
