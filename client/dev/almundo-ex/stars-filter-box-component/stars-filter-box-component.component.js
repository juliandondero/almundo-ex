;(function(ng) {
  'use strict';

  StarsFilterBoxCtrl.$inject=[];

  function StarsFilterBoxCtrl(){

    var vm = this;
    vm.stars=null;
    vm.all_stars = null;

    activate();

    function activate(){
      vm.stars = [true,true,true,true,true];
      vm.all_stars=true;
      vm.stars_values=[1,2,3,4,5];
    }


    vm.apply=function(){

      var stars=[];

      if (vm.all_stars){
        stars =[1,2,3,4,5];
      } else {

        _.each(vm.stars,function(marked,index){
          if (marked){
            stars.push(index+1);
          }
        });
      }

      vm.applyCallback({stars:stars});
    }


  }

  ng.module('stars-filter-box-component').component('starsFilterBox',{
    controller:StarsFilterBoxCtrl,
    controllerAs:'vm',
    templateUrl:'almundo-ex/stars-filter-box-component/stars-filter-box-component.template.html',
    bindings: {
      applyCallback:'&'
    }
  });

}(window.angular));
