;(function(ng) {
  'use strict';

  HeaderComponentCtrl.$inject=[];

  function HeaderComponentCtrl(){}

  ng.module('header-component').component('headerComponent',{
    controller:HeaderComponentCtrl,
    controllerAs:'vm',
    templateUrl:'almundo-ex/header-component/header-component.template.html',
    bindings:{}
  });

}(window.angular));
