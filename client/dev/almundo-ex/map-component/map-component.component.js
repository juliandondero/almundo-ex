;(function(ng) {
  'use strict';

  MapComponentCtrl.$inject=[];

  function MapComponentCtrl(){}

  ng.module('map-component').component('mapComponent',
    {
      controller: MapComponentCtrl,
      controllerAs:'vm',
      templateUrl:'almundo-ex/map-component/map-component.template.html',
      bindings:{}
    });

}(window.angular));
