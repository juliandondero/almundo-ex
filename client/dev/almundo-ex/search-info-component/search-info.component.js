;(function(ng) {
  'use strict';

  SearchInfoCtrl.$inject=[];

  function SearchInfoCtrl(){
    var vm = this;

  }

  ng.module('search-info-component').component('searchInfo',{
    controller:SearchInfoCtrl,
    controllerAs:'vm',
    templateUrl:'almundo-ex/search-info-component/search-info-component.template.html',
    bindings:{
      filters:'<'
    }
  });

}(window.angular));
