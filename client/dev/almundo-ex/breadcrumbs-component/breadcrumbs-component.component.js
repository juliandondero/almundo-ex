;(function(ng) {
  'use strict';

  BreadCrumbsComponentCtrl.$inject=[];

  function BreadCrumbsComponentCtrl(){}

  ng.module('breadcrumbs-component').component('breadcrumbsComponent',{
    controller:BreadCrumbsComponentCtrl,
    controllerAs:'vm',
    templateUrl:'almundo-ex/breadcrumbs-component/breadcrumbs-component.template.html',
    bindings:{}
  });

}(window.angular));
