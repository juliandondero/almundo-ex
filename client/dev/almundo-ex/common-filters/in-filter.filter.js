;(function(ng) {
  'use strict';

  InFilter.$inject=[];

  function InFilter(){
    return function(items,accepted_values,propertyName) {

      var filtered = [];

      // If time is in the accepted_values
      angular.forEach(items , function(item) {
        if (_.includes(accepted_values ,item[propertyName])){
          filtered.push(item);
        }
      });
      return filtered;
    };
  }

  ng.module('common-filters').filter('InFilter',InFilter);

}(window.angular));
