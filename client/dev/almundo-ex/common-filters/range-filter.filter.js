;(function(ng) {
  'use strict';

  RangeFilter.$inject=[];

  function RangeFilter(){
    return function(items, min, max,propertyName) {
      var filtered = [];
      var min = parseInt(min);
      var max = parseInt(max);

      // If time is with the range
      angular.forEach(items, function(item) {
        if( item[propertyName] >= min && item[propertyName] <= max ) {
          filtered.push(item);
        }
      });
      return filtered;
    };
  }

  ng.module('common-filters').filter('RangeFilter',RangeFilter);

}(window.angular));
