;(function(ng) {
  'use strict';

  CollapsableBoxCtrl.$inject=[];

  function CollapsableBoxCtrl(){

    var vm=this;
    vm.visible=null;

    activate();

    function activate(){
      vm.visible = true;
    }

    vm.toggleVisible=function(){
      vm.visible = !vm.visible;
    }


  }

  ng.module('collapsable-box-component').component('collapsableBox',{
    controller:CollapsableBoxCtrl,
    controllerAs:'vm',
    templateUrl:'almundo-ex/collapsable-box-component/collapsable-box-component.template.html',
    transclude:true,
    bindings:{
      title:'@',
      icon:'@'
    }
  });

}(window.angular));
