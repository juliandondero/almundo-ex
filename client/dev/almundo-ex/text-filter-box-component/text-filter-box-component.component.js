;(function(ng) {
  'use strict';

  TextFilterBoxCtrl.$inject=[];

  function TextFilterBoxCtrl(){

    var vm = this;

    vm.apply=function(){

      vm.applyCallback({text:vm.text});
      vm.text=null;
    }


  }

  ng.module('text-filter-box-component').component('textFilterBox',{
    controller:TextFilterBoxCtrl,
    controllerAs:'vm',
    templateUrl:'almundo-ex/text-filter-box-component/text-filter-box-component.template.html',
    bindings:{
      applyCallback:'&',
      inputPlaceholder:'@'
    }
  });

}(window.angular));
