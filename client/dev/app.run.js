;(function(ng) {
  'use strict';
  AppRunFn.$inject=[];

  function AppRunFn(){}

  ng.module('almundo-ex')
    .run(AppRunFn);

}(window.angular));
