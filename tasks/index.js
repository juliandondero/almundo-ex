
import gulp from 'gulp';
import {tasks} from './client/const';

gulp.task('default', [tasks.CLIENT_BUILD_SASS_DEV,tasks.CLIENT_WATCH,tasks.CLIENT_WATCH_SASS]);

require('require-dir')('client');

require('require-dir')('server');

