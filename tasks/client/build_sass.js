'use strict';

var sass = require('gulp-sass');
import gulp from 'gulp';
import {path, tasks} from './const';

const SCSS = [
  path.DEV + 'almundo-ex/**/*.scss',
  '!' + path.DEV + 'bower_components/**/*.css'
];

gulp.task(tasks.CLIENT_BUILD_SASS_DEV, () => {
  return gulp.src(SCSS)
        .pipe(sass().on('error', sass.logError))
             .pipe(gulp.dest(path.DEV+'almundo-ex/'));
});

gulp.task(tasks.CLIENT_WATCH_SASS, function () {
  gulp.watch(SCSS, [tasks.CLIENT_BUILD_SASS_DEV]);
});
