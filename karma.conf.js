'use strict';

module.exports = (config) => {
  config.set({

    // base path that will be used to resolve all patterns (eg. files, exclude)
    basePath: '',


    // frameworks to use
    // available frameworks: https://npmjs.org/browse/keyword/karma-adapter
    frameworks: ['jasmine'],


    // list of files / patterns to load in the browser
    files:
      [
        'client/dev/bower_components/jquery/dist/jquery.min.js',
        'client/dev/bower_components/angular/angular.min.js',
        'client/dev/bower_components/angular-resource/angular-resource.min.js',
        'client/dev/bower_components/angular-route/angular-route.min.js',
        'client/dev/bower_components/angular-messages/angular-messages.min.js',
        'client/dev/app.module.js',
        'client/dev/app.config.js',
        'client/dev/app.route.js',
        'client/dev/app.run.js',
        'client/dev/almundo-ex/common-services/common-services.module.js',
        'client/dev/almundo-ex/common-services/sort-methods.service.js',
        'client/dev/almundo-ex/common-filters/common-filters.module.js',
        'client/dev/almundo-ex/common-filters/range-filter.filter.js',
        'client/dev/almundo-ex/common-filters/in-filter.filter.js',
        'client/dev/almundo-ex/common-resources/common-resources.module.js',
        'client/dev/almundo-ex/common-resources/hotel.factory.js',
        'client/dev/almundo-ex/header-component/header-component.module.js',
        'client/dev/almundo-ex/header-component/header-component.component.js',
        'client/dev/almundo-ex/breadcrumbs-component/breadcrumbs-component.module.js',
        'client/dev/almundo-ex/breadcrumbs-component/breadcrumbs-component.component.js',
        'client/dev/almundo-ex/hotels-route/hotels-route.module.js',
        'client/dev/almundo-ex/hotels-route/hotels-route.routes.js',
        'client/dev/almundo-ex/hotels-route/hotels-route.component.js',
        'client/dev/almundo-ex/search-info-component/search-info.module.js',
        'client/dev/almundo-ex/search-info-component/search-info.component.js',
        'client/dev/almundo-ex/sorting-bar-component/sorting-bar-component.module.js',
        'client/dev/almundo-ex/sorting-bar-component/sorting-bar-component.component.js',
        'client/dev/almundo-ex/map-component/map-component.module.js',
        'client/dev/almundo-ex/map-component/map-component.component.js',
        'client/dev/almundo-ex/hotel-preview-component/hotel-preview-component.module.js',
        'client/dev/almundo-ex/hotel-preview-component/hotel-preview-component.component.js',
        'client/dev/almundo-ex/text-filter-box-component/text-filter-box-component.module.js',
        'client/dev/almundo-ex/text-filter-box-component/text-filter-box-component.component.js',
        'client/dev/almundo-ex/range-filter-box-component/range-filter-box-component.module.js',
        'client/dev/almundo-ex/range-filter-box-component/range-filter-box-component.component.js',
        'client/dev/almundo-ex/stars-filter-box-component/stars-filter-box-component.module.js',
        'client/dev/almundo-ex/stars-filter-box-component/stars-filter-box-component.component.js',
        'client/dev/almundo-ex/collapsable-box-component/collapsable-box-component.module.js',
        'client/dev/almundo-ex/collapsable-box-component/collapsable-box-component.component.js',
        'client/dev/almundo-ex/responsive-collapsable-box-component/responsive-collapsable-box-component.module.js',
        'client/dev/almundo-ex/responsive-collapsable-box-component/responsive-collapsable-box-component.component.js',
        'client/dev/!(bower_components)/**/*.js',
        'client/dev/bower_components/angular-mocks/angular-mocks.js',
        'tests/client/**/**/*_test.js',
        'client/dev/**/*.html' // for templateUrl testing
      ],

    // list of files to exclude
    exclude: [],

    // test results reporter to use
    // possible values: 'dots', 'progress'
    // available reporters: https://npmjs.org/browse/keyword/karma-reporter
    reporters: ['dots', 'coverage'],


    // preprocess matching files before serving them to the browser
    // available preprocessors: https://npmjs.org/browse/keyword/karma-preprocessor
    preprocessors:
    {
      'client/dev/!(bower_components)/**/*.js': ['coverage'],
      'client/dev/**/*.html': ['ng-html2js']
    },

    ngHtml2JsPreprocessor:
    {
      stripPrefix: 'client/dev/',
      moduleName: 'my.includes'
    },

    coverageReporter:
    {
      type : 'lcov',
      dir : 'unit_coverage/'
    },

    // web server port
    port: 9876,


    // enable / disable colors in the output (reporters and logs)
    colors: true,


    // level of logging
    // possible values: config.LOG_DISABLE || config.LOG_ERROR || config.LOG_WARN || config.LOG_INFO || config.LOG_DEBUG
    logLevel: config.LOG_INFO,


    // enable / disable watching file and executing tests whenever any file changes
    autoWatch: true,


    // start these browsers
    // available browser launchers: https://npmjs.org/browse/keyword/karma-launcher
    browsers: ['Chrome', 'Firefox', 'FirefoxNightly', 'ChromeCanary', 'IE', 'Safari', 'PhantomJS'],

    captureTimeout: 120000,

    // Continuous Integration mode
    // if true, Karma captures browsers, runs the tests and exits
    singleRun: true
  });
};
